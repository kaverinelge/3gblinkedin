<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Generic_Function {
		
		public function generate_code($table, $column, $prefix)
		{
			//Generate application_id for new entry
			$insert_application_id = '';
			
			$select = "RIGHT($column,5) as $column";
			$table = $table; 
			//commented by Amruta on 16 Jul 2019 $where = "is_deleted='N' and is_active='Y' AND $column like '".$prefix."%'";
			$where = " $column like '".$prefix."%'";
			$orderby = 'id'; 
			$ordervalue = 'DESC';
			$limit = "1";
			
			$CI =& get_instance();

			$CI->db->select($select);
			$CI->db->from($table); 
			$CI->db->where($where);
			$CI->db->limit("1");
			//$CI->db->group_by($group_by);
			$CI->db->order_by("id", "DESC"); 
			
			$query = $CI->db->get();
			
			if(count($query->result()) > 0)
			{
				$employee_id = $query->result();
				
				if(empty($employee_id[0]->$column))
					$insert_employee_id = $prefix."_00001";
				else
					$insert_employee_id = $prefix."_".str_pad(($employee_id[0]->$column + 1), 5, "0", STR_PAD_LEFT);

				return $insert_employee_id;
			}
			else
				return $insert_employee_id = $prefix."_00001";
			
		}

		public function get_address_by_lat_long($lat, $long)
		{
			$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" .
			$lat . "," . $long . "&key=AIzaSyCw3n7_wE06LGtgHyOhIAgdU7bA5Oo0lrU";
			$json = @file_get_contents($url);
			echo "inside generic function---";print_r($json);
			$data = json_decode($json);
			$status = $data->status;
			$address = '';
			if ($status == "OK") {   
			$address = $data->results[0]->formatted_address;
			}
			/*else if ($status == "OVER_QUERY_LIMIT")
			{
				$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" .
				$lat . "," . $long . "&key=AIzaSyD3NsPyAxrIDI9ia7-wpaVEL4jbmEjv9H0";
				$json = @file_get_contents($url);
				//print_r($json);
				$data = json_decode($json);
				$status = $data->status;
				$address = '';
				if ($status == "OK") {   
					$address = $data->results[0]->formatted_address;
				}
			}*/

			return $address;			
		}
		
	}	
