<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Select_record
	{
		public function select_single_table_data($coloum_name,$table,$where,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table);
			$CI->db->where($where);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			//echo $CI->db->last_query();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
				return "F";
			} 
		}
		
		
		public function select_single_table_data_two_orderby($coloum_name,$table,$where,$orderfieldkey1,$orderfieldvalue1,$orderfieldkey2,$orderfieldvalue2)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table);
			$CI->db->where($where);
			$CI->db->order_by($orderfieldkey1,$orderfieldvalue1); 
			$CI->db->order_by($orderfieldkey2,$orderfieldvalue2); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
				return "F";
			} 
		}
		
		
		public function select_single_table_data_limit($coloum_name,$table,$where,$orderfieldkey,$orderfieldvalue,$limit)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table);
			$CI->db->where($where);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$CI->db->limit($limit);  
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
				return "F";
			} 
		}
		
		
		
		public function select_max_id($coloum_name,$alise_name,$table,$where)
		{
			$CI =& get_instance();
			if($where!='')
			{
				$query = $CI->db->query("select max($coloum_name) as $alise_name from $table where $where");
			}
			else
			{	
				$query = $CI->db->query("select max($coloum_name) as $alise_name from $table");
			}
			
			/* $this->db->select_max($coloum_name as $alise_name);
				$this->db->where($where);
			$query = $this->db->get($table);  */
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
				return "F";
			} 
		}
		
		public function select_single_whereIn_table_data($coloum_name,$table,$where,$wherein_key,$wherein_value,$orderfieldkey,$orderfieldvalue)
		{
			
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table);
			$CI->db->where($where);
			$CI->db->where_in($wherein_key,$wherein_value);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			//echo $CI->db->last_query();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
				return "F";
			} 
		}
		
		
		public function select_single_2whereIn_table_data($coloum_name,$table,$where,$wherein_key1,$wherein_value1,$wherein_key2,$wherein_value2,$orderfieldkey,$orderfieldvalue)
		{
			
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table);
			$CI->db->where($where);
			$CI->db->where_in($wherein_key1,$wherein_value1);
			$CI->db->where_in($wherein_key2,$wherein_value2);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			//echo $CI->db->last_query();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
				return "F";
			} 
		}
		
		/* public function select_single_autocomplete_table_data($coloum_name,$table,$like_key,$like_value,$where)
			{
			
			$CI =& get_instance();
			$CI->db->distinct();
			$CI->db->select($coloum_name);
            $CI->db->from($table);
			$CI->db->like($like_key, $like_value,'both');
			$CI->db->where($where);
			$query = $CI->db->get();
			//echo $CI->db->last_query();
			if($query->num_rows() != 0)
            {
			return $query->result();
			}
            else
            {
			return "F";
			} 
			}
		*/
		
		
		public function select_single_table_Groupby_data($coloum_name,$table1,$where,$group_by,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->where($where);
			$CI->db->group_by($group_by);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue);
			$query = $CI->db->get();
			//echo $CI->db->last_query();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
		}
		
		public function select_single_table_WhereIn_Groupby_data($coloum_name,$table1,$where,$wherein_key,$wherein_value,$group_by,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->where($where);
			$CI->db->where_in($wherein_key,$wherein_value);
			$CI->db->group_by($group_by);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
		}
		public function select_two_table_data($coloum_name,$table1,$table2,$condition1,$where,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->where($where);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
		}
		
		
		public function select_two_table_Groupby_data($coloum_name,$table1,$table2,$condition1,$where,$group_by,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->where($where);
			$CI->db->group_by($group_by);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
		}
		
		public function select_two_table_WhereIn_data($coloum_name,$table1,$table2,$condition1,$where,$wherein_key,$wherein_value,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->where($where);
			$CI->db->where_in($wherein_key,$wherein_value);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
		}
		
		
		public function select_two_table_WhereIn_Groupby_data($coloum_name,$table1,$table2,$condition1,$where,$wherein_key,$wherein_value,$group_by,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->where($where);
			$CI->db->where_in($wherein_key,$wherein_value);
			$CI->db->group_by($group_by);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
		}
		
		public function select_two_table_2WhereIn_Groupby_data($coloum_name,$table1,$table2,$condition1,$where,$wherein_key1,$wherein_value1,$wherein_key2,$wherein_value2,$group_by,$orderfieldkey,$orderfieldvalue)
		{
			
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->where($where);
			$CI->db->where_in($wherein_key1,$wherein_value1);
			$CI->db->where_in($wherein_key2,$wherein_value2);
			$CI->db->group_by($group_by);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
			
		}
		
		
		public function select_three_table_data($coloum_name,$table1,$table2,$table3,$condition1,$condition2,$where,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->join($table3, $condition2);
			$CI->db->where($where);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
				return "F";
			} 
		}
		
		public function select_three_table_Groupby_data($coloum_name,$table1,$table2,$table3,$condition1,$condition2,$where,$group_by,$orderfieldkey,$orderfieldvalue)
		{
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->join($table3, $condition2);
			$CI->db->where($where);
			$CI->db->group_by($group_by);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			//echo "<br>".$CI->db->last_query();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
		}
		
		public function select_four_table_2WhereIn_Groupby_data($coloum_name,$table1,$table2,$table3,$table4,$condition1,$condition2,$condition3,$where,$wherein_key1,$wherein_value1,$wherein_key2,$wherein_value2,$group_by,$orderfieldkey,$orderfieldvalue)
		{
			
			$CI =& get_instance();
			$CI->db->select($coloum_name);
            $CI->db->from($table1);
			$CI->db->join($table2, $condition1);
			$CI->db->join($table3, $condition2);
			$CI->db->join($table4, $condition3);
			$CI->db->where($where);
			$CI->db->where_in($wherein_key1,$wherein_value1);
			$CI->db->where_in($wherein_key2,$wherein_value2);
			$CI->db->group_by($group_by);
			$CI->db->order_by($orderfieldkey,$orderfieldvalue); 
			$query = $CI->db->get();
			if($query->num_rows() != 0)
            {
                return $query->result();
			}
            else
            {
                return "F";
			} 
			
		}
		
		
	}
?>