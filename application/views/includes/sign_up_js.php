

<script src="<?php echo base_url();?>theme/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="<?php echo base_url();?>theme/assets/node_modules/popper/popper.min.js"></script>

<script src="<?php echo base_url();?>theme/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<script type="text/javascript">
	
	$(function() {
		
		$(".preloader").fadeOut();
		
	});
	
	$(function() {
		
		$('[data-toggle="tooltip"]').tooltip()
		
	});
	
	// ============================================================== 
	
	// Login and Recover Password 
	
	// ============================================================== 
	
	$('#to-recover').on("click", function() {
		
		$("#loginform").slideUp();
		
		$("#recoverform").fadeIn();
		
	});
	
</script>