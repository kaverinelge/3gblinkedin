<script src="<?php echo base_url();?>theme/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>

<!-- Bootstrap tether Core JavaScript -->

<script src="<?php echo base_url();?>theme/assets/node_modules/popper/popper.min.js"></script>

<script src="<?php echo base_url();?>theme/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- slimscrollbar scrollbar JavaScript -->

<script src="<?php echo base_url();?>theme/dist/js/perfect-scrollbar.jquery.min.js"></script>

<!--Wave Effects -->

<script src="<?php echo base_url();?>theme/dist/js/waves.js"></script>

<!--Menu sidebar -->

<script src="<?php echo base_url();?>theme/dist/js/sidebarmenu.js"></script>

<!--stickey kit -->

<script src="<?php echo base_url();?>theme/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>

<script src="<?php echo base_url();?>theme/assets/node_modules/sparkline/jquery.sparkline.min.js"></script>

<!--Custom JavaScript -->

<script src="<?php echo base_url();?>theme/dist/js/custom.min.js"></script>
<script src="<?php echo base_url();?>theme/assets/node_modules/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>theme/assets/node_modules/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script>
 $(function () {
$(".select2").select2();
 });
</script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url();?>theme/assets/node_modules/moment/moment.js"></script>
    <script src="../assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="<?php echo base_url();?>theme/assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="<?php echo base_url();?>theme/assets/node_modules/jquery-asColor/dist/jquery-asColor.js"></script>
    <script src="<?php echo base_url();?>theme/assets/node_modules/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="<?php echo base_url();?>theme/assets/node_modules/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="<?php echo base_url();?>theme/assets/node_modules/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo base_url();?>theme/assets/node_modules/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url();?>theme/assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="<?php echo base_url();?>theme/assets/node_modules/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>theme/dist/js/buttons.print.min.js"></script>
<script>
$('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy'
    });
</script>
<script>
$('.myTable').DataTable( {
	dom: 'Bfrtip',
	buttons: [
	'excel'
	]
});
</script>