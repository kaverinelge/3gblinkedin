

<style>
	/*.sidebar{position: fixed;
    overflow-y: scroll; z-index:10001;}
 	nav.navbar.navbar-default.navbar-static-top.m-b-0{position:fixed; z-index:10002;}
	*/
 .sidebar {
    background: #6c7a88;
    box-shadow: 1px 0px 20px rgba(0, 0, 0, 0.08);
   }
   .navbar-header{
	   background: #20b5ae !important;
   }
   .navbar-collapse{
	   background: #20b5ae !important;
   }
   .hide-menu{
	   color: #ffffff !important;
   }
#side-menu li a {
    color: #ffffff;
    border-left: 0px solid #fff;
  }
#side-menu > li > a.active {
    border-left: 3px solid #03a9f3;
    color: #4191b5;
    font-weight: 500; background-color: #fff; 
  }
#side-menu ul > li > a.active {
    color: #2b2b2b;
    font-weight: 500;  
    background: #fff;
 }
#side-menu > li > a:hover, #side-menu > li > a:focus {
    background: rgb(255, 255, 255);
    color: #4191b5;
  }
  @media (min-width: 768px){
.content-wrapper .sidebar .nav-second-level > li > a {
    padding-left: 30px;
    background: #272829;
}
}
  
 /* ----------- Non-Retina Screens ----------- */
@media screen 
  and (min-device-width: 1200px) 
  and (max-device-width: 1600px) 
  and (-webkit-min-device-pixel-ratio: 1)
  {   
  .slimScrollDiv {height:auto !important;}
	/*  .sidebar { width: 206px !important;} */
  } 


</style> 
<aside class="left-sidebar sidebar">
	
	<!-- Sidebar scroll-->
	
	<div class="scroll-sidebar">
		
		<!-- Sidebar navigation-->
		
		<nav class="sidebar-nav">
			
			<ul id="sidebarnav" id="side-menu">
			<li>
						<a href="Hubspot_CI">
							<i class=""></i>
							<span class="hide-menu">Hubspot Sheet</span>
							
						</a>
						
					</li>
			
			</ul>
			
		</nav>
		
		<!-- End Sidebar navigation -->
		
	</div>
	
	<!-- End Sidebar scroll-->
	
</aside>