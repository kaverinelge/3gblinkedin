<style>
.skin-blue .topbar, .skin-blue-dark .topbar {
    background: #1370a7;
}
</style>
<header class="topbar">
	
	<nav class="navbar top-navbar navbar-expand-md navbar-dark">
		
		<!-- ============================================================== -->
		
		<!-- Logo -->
		
		<!-- ============================================================== -->
		
		<div class="navbar-header" style="text-align: center;">
			
			<a class="navbar-brand" href="">
				
				<!-- Logo icon --><b>
					
					<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
					
					<!-- Dark Logo icon -->
					
					<!--<img src="<?php echo base_url();?>" alt="ECMS" class="dark-logo" />-->
					
					<!-- Light Logo icon -->
					
					<img src="<?php echo base_url();?>theme/assets/images/logo-icon.png" alt="ECMS" class="light-logo" />
					<!--<img src="<?php echo base_url();?>" alt="ECMS" class="light-logo" style="width: 42px;"/><h1>PCMC</h1>-->
					
				</b>
				
				<!--End Logo icon -->
				
				<!-- Logo text --><span>
					
					<!-- dark Logo text -->
					
					<!--<img src="<?php echo base_url();?>" alt="ECMS" class="dark-logo" />-->
					
					<!-- Light Logo text -->    
					
				<!--<img src="<?php echo base_url();?>theme/assets/images/logo-light-text.png" class="light-logo" alt="ECMS" /></span> -->
				<!--<img src="<?php echo base_url();?>" alt="ECMS" class="light-logo" style="width: 156px;"/>-->
				</a>
				
		</div>
		
		<!-- ============================================================== -->
		
		<!-- End Logo -->
		
		<!-- ============================================================== -->
		
		<div class="navbar-collapse">
			
			<!-- ============================================================== -->
			
			<!-- toggle and nav items -->
			
			<!-- ============================================================== -->
			
			<ul class="navbar-nav mr-auto">
				
				<!-- This is  -->
				
				<li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
				
				<li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
				
				<!-- ============================================================== -->
				
				<!-- Search -->
				
				<!-- ============================================================== -->
				
				<li class="nav-item">
					
					<form class="app-search d-none d-md-block d-lg-block">
						
						<input type="text" class="form-control" placeholder="Search & enter">
						
					</form>
					
				</li>
				
			</ul>
			
			<!-- ============================================================== -->
			
			<!-- User profile and search -->
			
			<!-- ============================================================== -->
			
			<ul class="navbar-nav my-lg-0">
				
				
				<li class="nav-item dropdown u-pro">
					
					<a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!--<img src="<?php echo base_url();?>theme/assets/images/users/1.jpg" alt="user" class=""> --><span class="hidden-md-down" style="color: white;"><?php echo $NAME="ECS";?> &nbsp;<i class="fa fa-angle-down"></i></span> </a>
					
					<!--<div class="dropdown-menu dropdown-menu-right animated flipInY">
						
						
						
						<a href="<?php echo base_url()?>Login_CI/Change_password" class="dropdown-item"><i class="fa fa-key"></i> Change Password</a>
						
<a href="<?php echo base_url()?>Login_CI/logout" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>						
						
						
						
					</div>-->
					
				</li>
				
				<!-- ============================================================== -->
				
				<!-- End User Profile -->
				
				<!-- ============================================================== -->
				
				<!--<li class="nav-item right-side-toggle"> <a class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>-->
				
			</ul>
			
		</div>
		
	</nav>
	
</header>