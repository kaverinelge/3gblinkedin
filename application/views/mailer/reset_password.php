<!DOCTYPE html>

<html lang="en">
	
	
	
	<head>
		
		<meta charset="utf-8">
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<!-- Tell the browser to be responsive to screen width -->
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<meta name="description" content="">
		
		<meta name="author" content="">
		
		<!-- Favicon icon -->
		
		<?php $this->load->view('includes/favicon.php');?>
		
		<title>ECMS</title>
		
		<?php $this->load->view('includes/sign_up_css.php');?>
		
		
		
		
		
	</head>
	
	
	
	<body class="skin-blue card-no-border">
		
		<!-- ============================================================== -->
		
		<!-- Preloader - style you can find in spinners.css -->
		
		<!-- ============================================================== -->
		
		<?php $this->load->view('includes/preloader.php');?>
		
		<!-- ============================================================== -->
		
		<!-- Main wrapper - style you can find in pages.scss -->
		
		<!-- ============================================================== -->
		
		<section id="wrapper">
			
			<div class="login-register" style="background-image:url(../theme/assets/images/background/login-register.jpg);">
				
				<div class="login-box card">
					
					<div class="card-body">
						
						<form class="form-horizontal form-material" id="resetform" action="<?php echo base_url();?>Login_CI/forgetpassword_action"  method="POST">
							
							<h3 class="box-title m-b-20">Enter New Password</h3>
							
							<div class="form-group ">
								
								<div class="col-xs-12">
									
									
									<input type="hidden" name="hidden_id" id="hidden_id" value="<?php echo $id;?>">
									<input class="form-control" type="password" required="" placeholder="New Password" name="password" id="password">
								</div>
								
							</div>
							<div class="form-group ">
								
								<div class="col-xs-12">

									<input class="form-control" type="password" required="" placeholder="Confirm New Password" name="confirm_password" id="confirm_password">
								</div>
								
							</div>

							<div class="form-group text-center">
								
								<div class="col-xs-12 p-b-20">
									
									<button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Save Password</button>
									
								</div>
								<a href="<?php echo base_url();?>" id="to-recover" class="text-dark pull-right"><i class="fa fa-arrow-left m-r-5"></i>Cancel</a> 
							</div>
							
							
							
						</form>
						
						
						
					</div>
					
				</div>
				
			</div>
			
		</section>
		
		
		
		<!-- ============================================================== -->
		
		<!-- End Wrapper -->
		
		<!-- ============================================================== -->
		
		<!-- ============================================================== -->
		
		<!-- All Jquery -->
		
		<!-- ============================================================== -->
		<?php $this->load->view('includes/login_js');?>
		
		
		
		
	</body>
	
	
	
</html>

    <script>
        $("#resetform").submit(function () {
            var formID = $(this).attr('id');
            var formDetails = $('#' + formID);

            $.ajax({
                type: "POST",
                url: '<?php echo base_url() ?>Login_CI/reset_password_action',
                dataType: "json",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (result) {
                    if (result.status == false)
                    {
						alert(result.message);
                    } else {
						alert(result.message);
                        window.setTimeout(function () {
                            window.location.href = '<?php echo base_url(); ?>';
                        }, 2500);
                    }
                },
                error: function (jqXHR, text, error) {
                    $('#result').html(error);
                    alert("ERROR");
                }
            });
            return false;
        });

    </script>