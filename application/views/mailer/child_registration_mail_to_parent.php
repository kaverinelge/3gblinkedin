<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #f5f8fa; min-width: 350px; font-size: 1px; line-height: normal;">
    <tr>
        <td align="center" valign="top">
            <!--[if (gte mso 9)|(IE)]>
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" valign="top" width="750">
                  <![endif]-->
            <table cellpadding="0" cellspacing="0" border="0" width="750" class="table750"
                   style="width: 100%; max-width: 750px; min-width: 350px; background: #f5f8fa;">
                <tr>
                    <td class="mob_pad" width="25" style="width: 25px; max-width: 25px; min-width: 25px;">&nbsp;</td>
                    <td align="center" valign="top" style="background: #ffffff;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;">
                            <tr>
                                <td align="right" valign="top">
                                    <div class="top_pad" style="height: 25px; line-height: 25px; font-size: 23px;">&nbsp;</div>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="88%" style="width: 88% !important; min-width: 88%; max-width: 88%;">
                            <tr>
                                <td align="left" valign="top">

                                    <div style="height: 21px; line-height: 21px; font-size: 19px;">&nbsp;</div> <font face="'Source Sans Pro', sans-serif" color="#000000" style="font-size: 20px; line-height: 28px;">
                                    <span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;">
                                        Dear Parents <?php echo $parent_name ?>,
                                    </span>
                                    </font>

                                    <div style="height: 6px; line-height: 6px; font-size: 4px;">&nbsp;</div> <font face="'Source Sans Pro', sans-serif" color="#000000" style="font-size: 20px; line-height: 28px;">
                                    <span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;">
                                        We are happy to inform you that your child <?php echo $child_name; ?> has been enrolled successfully in our centre <?php echo $center_name;?>
                                    </span>
                                    </font>
									<div style="height: 6px; line-height: 6px; font-size: 4px;">&nbsp;</div>
									<font face="'Source Sans Pro', sans-serif" color="#000000" style="font-size: 20px; line-height: 28px;">
                                    <span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;">
                                        Name of Child : <?php echo $child_name ?>
                                    </span>
                                    </font>		
									<div style="height: 6px; line-height: 6px; font-size: 4px;">&nbsp;</div>
									<font face="'Source Sans Pro', sans-serif" color="#000000" style="font-size: 20px; line-height: 28px;">
                                    <span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;">
                                        Type of Service : <?php echo $service_type ?>
                                    </span>
                                    </font>										
									<div style="height: 6px; line-height: 6px; font-size: 4px;">&nbsp;</div>
									<font face="'Source Sans Pro', sans-serif" color="#000000" style="font-size: 20px; line-height: 28px;">
                                    <span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;">
                                        Programme Fee : <?php echo $fee ?>
                                    </span>
                                    </font>																			

                                    <div style="height: 30px; line-height: 30px; font-size: 28px;">&nbsp;</div>
                                    <div style="height: 90px; line-height: 90px; font-size: 88px;">&nbsp;</div>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="90%" style="width: 90% !important; min-width: 90%; max-width: 90%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-bottom: none; border-left: none; border-right: none;">
                            <tr>
                                <td align="left" valign="top">
                                    <div style="height: 28px; line-height: 28px; font-size: 26px;">&nbsp;</div>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="88%" style="width: 88% !important; min-width: 88%; max-width: 88%;">
                            <tr>
                                <td align="left" valign="top"> <font face="'Source Sans Pro', sans-serif" color="#7f7f7f" style="font-size: 17px; line-height: 23px;">
                                    <span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #7f7f7f; font-size: 17px; line-height: 23px;">
									Please feel free to contact centre should you require any other details.
									</span>
                                    </font>

                                    <div style="height: 30px; line-height: 30px; font-size: 28px;">&nbsp;</div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top"> <font face="'Source Sans Pro', sans-serif" color="#7f7f7f" style="font-size: 17px; line-height: 23px;">
                                    <span style="font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #7f7f7f; font-size: 17px; line-height: 23px;">
									Thanks <br> For Centre <br> <?php echo $center_name; ?>
									</span>
                                    </font>

                                    <div style="height: 30px; line-height: 30px; font-size: 28px;">&nbsp;</div>
                                </td>
                            </tr>
                        </table>
                    <td class="mob_pad" width="25" style="width: 25px; max-width: 25px; min-width: 25px;">&nbsp;</td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
          </tr>
        </table>
      <![endif]-->
        </td>
    </tr>
</table>
