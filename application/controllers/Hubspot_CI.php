<?php
defined("BASEPATH") OR exit("No direct script access allowed");
error_reporting(0);	
class Hubspot_CI extends CI_Controller {
	
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper("date");
		$this->load->helper("url");  
		$this->Generic_Function = new Generic_Function();
		$this->ApplicationID = "TGB";
		
	}
	
	
	public function index()
	{  
		
		$data["resultfetchdata"]=$this->Hubspot_Model->get_All_data();
		$data["form_heading"]="Hubspot Import Sheet";
		$this->load->view("Hubspot_View", $data);
	}
	
	public function save_dynamic_master_data()
	{			

		$this->load->library("form_validation");

		 $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

		  if ($_POST['check'] != '') {

            $this->form_validation->set_rules('check', 'check', 'trim|required');

        }

		$this->form_validation->set_rules('LinkedIn_Source', 'LinkedIn Source', "trim|required");
		$this->form_validation->set_rules('First_Name', 'First Name', "trim|required");
		$this->form_validation->set_rules('Last_Name', 'Last Name', "trim|required");
		$this->form_validation->set_rules('Company_Domain_Name', 'Company Domain Name', "trim|required");
		$this->form_validation->set_rules('Email', 'email address', 'trim|required|valid_email');
		$this->form_validation->set_rules('LinkedIn_URL', 'LinkedIn URL', "trim|required");
		$this->form_validation->set_rules('Platform', 'LinkedIn Source', "trim|required");
		$this->form_validation->set_rules('Comapaign', 'Comapaign', "trim|required");
		$this->form_validation->set_rules('Job_Title', 'Job Title', "trim|required");
		$this->form_validation->set_rules('Industry', 'Industry', "trim|required");
		$this->form_validation->set_rules('Number_of_Employes', 'Number of Employes', "trim|required");
		$this->form_validation->set_rules('Internal_IT', 'Internal IT', "trim|required");
		$this->form_validation->set_rules('Lead_Status', 'Lead Status', "trim|required");
		$this->form_validation->set_rules('Lead_Score', 'Lead Score', "trim|required");
		$this->form_validation->set_rules('Note', 'Note', "trim|required");
		$this->form_validation->set_rules('Lifecycle_stage', 'LinkedIn Source', "trim|required");
		$this->form_validation->set_rules("Mobile_Phone_No", "Mobile No.", "trim|required|min_length[10]|max_length[10]");

		
		if ($this->form_validation->run() == FALSE)
		{	
			
			
			$data["form_heading"]="Hubspot Import Sheet";
			$this->load->view("Hubspot_View", $data);
		}
		else
		{

			$result = $this->Hubspot_Model->save_dynamic_master_data();

			if ($result != 'Insertfalse' && $result != 'Updatetrue' && $result != 'Updatefalse') {

				$this->session->set_flashdata('success_msg', 'Record Inserted');
			}

			 if ($result == 'Insertfalse') {



                $this->session->set_flashdata('success_msg', 'Record Not Inserted');

            }

            if ($result == 'Updatetrue') {

                $this->session->set_flashdata('success_msg', 'record updated');

            }

            if ($result == 'Updatefalse') {

                $this->session->set_flashdata('success_msg', 'Record Not Updated');

            }


			    redirect("Hubspot_CI", "refresh");
		}
	}
	
	public function edit_master_data($id) 
	{
		$id=base64_decode($id);
		
		$data["form_heading"]=$this->form_name;
		$data["EditMasterDetails"] = $this->Hubspot_Model->get_edit_data($id);
		
		//$data["DynamicMultipleAddRemoveDetails"] = $this->User_master_model->getDynamicMultipleAddRemoveDetails($id);	
		$this->load->view("Hubspot_View", $data);
		
		
	} 
	
	public function delete_master_data($id)
	{
		$id=base64_decode($id);
		$result = $this->Hubspot_Model->delete_master_data($id);
		if($result==1)
		{
			echo "success";
			
		}
		if($result==0)
		{
			echo "fail";
		}
		
		
	}

	

}
?>