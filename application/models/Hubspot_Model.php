<?php
	class Hubspot_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->ApplicationID = "TGB";
		
		date_default_timezone_set("Asia/Kolkata");
		
	}

	public function get_All_data()
	{
	   	
		return $this->select_record->select_single_table_data("*","tbl_hubspot_master","is_deleted='N'","id","DESC");
	} 

	public function get_edit_data($id)
	{
		$where="is_deleted='N' and id = ".$id;

		$query = $this->db->get_where("tbl_hubspot_master",$where); 

		echo $this->db->last_query();

		return $query->result();
	   	
		
	} 
	
	public function save_dynamic_master_data(){

		 if ($_POST['check'] == '') {
	            $insertdata = array(
               
                'First_Name' => $this->input->post('First_Name'),
                'Last_Name' => $this->input->post('Last_Name'),
                'Company_Domain_Name' => $this->input->post('Company_Domain_Name'),
                'Email' => $this->input->post('Email'),
                'Phone_No' => $this->input->post('Phone_No'),
                'Mobile_Phone_No' => $this->input->post('Mobile_Phone_No'),
                'LinkedIn_URL' => $this->input->post('LinkedIn_URL'),
                'LinkedIn_Source' => $this->input->post('LinkedIn_Source'),
                'Platform' => $this->input->post('Platform'),
                'Comapaign' => $this->input->post('Comapaign'),
                'Job_Title' => $this->input->post('Job_Title'),
                'Industry' => $this->input->post('Industry'),
                'Number_of_Employes' => $this->input->post('Number_of_Employes'),
                //'fm_social_history' =>  $_POST['fm_social_history'],
                'Internal_IT' => $this->input->post('Internal_IT'),
                'Lead_Status' => $this->input->post('Lead_Status'),
                'Lead_Score' => $this->input->post('Lead_Score'),
                'Note' => $this->input->post('Note'),
                'Lifecycle_stage' => $this->input->post('Lifecycle_stage'),
                'IS_DELETED' => 'N',
                'IS_ACTIVE' => 'Y',
                'APPLICATION_ID' => 'TGB',
                'CREATED_TIME' => date("Y-m-d H:i:s"),
                'CREATED_BY' => '1',
                'SOURCE' => 'web');
               

            //print_r($insertdata);

           $insert= $this->db->insert('tbl_hubspot_master', $insertdata);

				$insert_id = $this->db->insert_id();

            //echo $this->db->last_query();



            if ($this->db->affected_rows() != 1) {

                return 'Insertfalse';
            } else {

                return 'Inserttrue'.$insert_id;
            }
        }
        else{

            $updatedata = array(
                 'First_Name' => $this->input->post('First_Name'),
                'Last_Name' => $this->input->post('Last_Name'),
                'Company_Domain_Name' => $this->input->post('Company_Domain_Name'),
                'Email' => $this->input->post('Email'),
                'Phone_No' => $this->input->post('Phone_No'),
                'Mobile_Phone_No' => $this->input->post('Mobile_Phone_No'),
                'LinkedIn_URL' => $this->input->post('LinkedIn_URL'),
                'LinkedIn_Source' => $this->input->post('LinkedIn_Source'),
                'Platform' => $this->input->post('Platform'),
                'Comapaign' => $this->input->post('Comapaign'),
                'Job_Title' => $this->input->post('Job_Title'),
                'Industry' => $this->input->post('Industry'),
                'Number_of_Employes' => $this->input->post('Number_of_Employes'),
                //'fm_social_history' =>  $_POST['fm_social_history'],
                'Internal_IT' => $this->input->post('Internal_IT'),
                'Lead_Status' => $this->input->post('Lead_Status'),
                'Lead_Score' => $this->input->post('Lead_Score'),
                'Note' => $this->input->post('Note'),
                'Lifecycle_stage' => $this->input->post('Lifecycle_stage'),
                'IS_DELETED' => 'N',
                'IS_ACTIVE' => 'Y',
                'APPLICATION_ID' => 'TGB',
                'UPDATED_TIME' => date("Y-m-d H:i:s"),
                'UPDATED_BY' => '1',
                'SOURCE' => 'web'
            );

            //print_r($updatedata);

            $this->db->where('id', $_POST['check']);



            $this->db->update('tbl_hubspot_master', $updatedata);
            if ($this->db->affected_rows() != 1) {

                return 'Updatefalse';
            } else {

                //return  'Updatetrue';

                return 'Updatetrue';
            }
        }
    }

    public function delete_master_data($delid)

		{

			$deletedata = array('IS_DELETED' =>'Y');

			//$this->db->where('id',$this->encrypt->decode($delid));

			
			$this->db->where('id',$delid);

			$this->db->update('tbl_hubspot_master', $deletedata);

			

			if($this->db->affected_rows() != 1)

			{

				return '0';

			}

			else{

				return  '1';

			}

		} 

	
}
?>